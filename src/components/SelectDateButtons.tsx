import { Button, Drawer, IconButton } from "rsuite";
import { CurrentDate } from "./CurrentDay";
import { ArrowLeftLine, ArrowRightLine } from "@rsuite/icons";

import styles from "./SelectDateButtons.module.css";
import { useState } from "react";
import CompactCalendar from "./CompactCalendar";
import { Schedule } from "@/models";
import { SchedulesTags } from "./SchedulesTags";
import { useSwipeable } from "react-swipeable";

export const SelectDateButtons = ({
  date,
  setDate,
  schedules,
}: {
  date: Date;
  setDate: (date: Date) => void;
  schedules: Schedule[];
}) => {
  const [isCalendarOpen, setIsCalendarOpen] = useState(false);

  const goNext = () => {
    const copy = new Date(date);
    copy.setDate(copy.getDate() + 1);
    setDate(copy);
  };

  const goBack = () => {
    const copy = new Date(date);
    copy.setDate(copy.getDate() - 1);
    setDate(copy);
  };

  const handlers = useSwipeable({
    onSwipedDown: () => setIsCalendarOpen(false),
    trackMouse: true,
    trackTouch: true,
  });

  return (
    <div className={styles.buttons}>
      <IconButton
        style={{ width: "50px" }}
        onClick={goBack}
        icon={<ArrowLeftLine />}
      ></IconButton>
      <Button onClick={() => setIsCalendarOpen(true)}>
        <CurrentDate date={date} size="md" />
      </Button>
      <IconButton
        style={{ width: "50px" }}
        onClick={goNext}
        icon={<ArrowRightLine />}
      ></IconButton>
      <Drawer
        size="lg"
        placement="bottom"
        open={isCalendarOpen}
        onClose={() => setIsCalendarOpen(false)}
        {...handlers}
      >
        <Drawer.Header>
          <SchedulesTags schedules={schedules} />
        </Drawer.Header>
        <Drawer.Body>
          <div className={styles.body}>
            <CompactCalendar
              currentDate={date}
              onSelectDateEvent={setDate}
              schedules={schedules}
            />
          </div>
        </Drawer.Body>
      </Drawer>
    </div>
  );
};
