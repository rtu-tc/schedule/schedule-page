import React, { useRef, useEffect } from "react";
import { Popover, Whisper } from "rsuite";
import FullCalendar from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import ruLocale from "@fullcalendar/core/locales/ru";
import { Schedule } from "@/models";
import { scheduleColorProvider } from "@/utils/ScheduleColorProvider";

import styles from "./TimeLine.module.css";

type TimeLineProps = {
  schedules: Schedule[];
  date: Date;
};

const getEventDescription = (description: string) => {
  const trimmedTeacher = description.replace("Преподаватель: ", "");
  const lineBreakPosition = trimmedTeacher.indexOf("\n");
  if (
    lineBreakPosition == -1 ||
    lineBreakPosition == trimmedTeacher.length - 1
  ) {
    return trimmedTeacher;
  }
  return `${trimmedTeacher.substring(0, lineBreakPosition - 1)}...`;
};

export const TimeLine = ({ schedules, date }: TimeLineProps) => {
  const calendarRef = useRef<FullCalendar | null>();

  const renderEventContent = (eventInfo: any) => {
    const getLocation = () => {
      let online_link = eventInfo.event.extendedProps.online_link;
      if (online_link) {
        if (online_link.indexOf("http") == -1) {
          online_link = "http://" + online_link;
        }
        return (
          <a href={online_link} target="_blank">
            {" "}
            {online_link}{" "}
          </a>
        );
      } else {
        return eventInfo.event.extendedProps.location;
      }
    };

    return (
      <Whisper
        enterable
        trigger="hover"
        placement="bottomStart"
        speaker={
          <Popover title={eventInfo.event.title}>
            <div
              style={{
                whiteSpace: "pre-wrap",
                maxHeight: "30vh",
                overflow: "auto",
              }}
            >
              <strong>{getLocation()}</strong>
              <div>{eventInfo.event.extendedProps.description}</div>
            </div>
          </Popover>
        }
      >
        <div className={styles.fullcalendarText}>
          <strong className={styles.eventTitle}>
            {eventInfo.timeText} {eventInfo.event.title}
          </strong>
          <div style={{ whiteSpace: "nowrap" }}>
            <strong>{getLocation()}</strong>{" "}
            {getEventDescription(eventInfo.event.extendedProps.description)}
          </div>
        </div>
      </Whisper>
    );
  };

  const eventsForFullCalendar = () => {
    const eventsForFullCalendarElement =
      schedules
        .filter((e) => e.calendar)
        .map((e, j) => {
          const elementCal = e.calendar!.getEventsForDay(date);
          const masElementCal = elementCal.map((element) => {
            return {
              id: element.id,
              backgroundColor: scheduleColorProvider(j),
              borderColor: scheduleColorProvider(j),
              textColor: "#272c36",
              title: element.title,
              start: element.start,
              end: element.end,
              extendedProps: {
                description: element.description,
                location: element.location,
                online_link: element.online_link,
              },
              allDay: element.allDay,
            };
          });
          return masElementCal;
        }) || [];

    // оставляем только уникальные объекты (для all day event-ов)
    const masEvent: Map<string, any> = new Map();
    eventsForFullCalendarElement.forEach((element) => {
      element.forEach((e) => masEvent.set(e.id, e));
    });
    return Array.from(masEvent.values());
  };

  useEffect(() => {
    calendarRef.current?.getApi().gotoDate(date);
  }, [date]);
  const eventsToRender = eventsForFullCalendar();
  const anyIsAllDay = !!eventsToRender.find((e) => e.allDay);
  return (
    <>
      <FullCalendar
        ref={(cr) => (calendarRef.current = cr)}
        plugins={[timeGridPlugin]}
        slotMinTime={"09:00:00"}
        slotMaxTime={"23:00:00"}
        initialView="timeGridDay"
        headerToolbar={{
          start: "",
          center: "",
          right: "",
        }}
        weekends={true}
        nowIndicator={true}
        eventContent={renderEventContent}
        events={eventsToRender}
        locale={ruLocale}
        eventOrder={["start"]}
        now={date}
        height="auto"
        allDaySlot={anyIsAllDay}
        allDayText={""}
      />
    </>
  );
};
export default TimeLine;
