import { Schedule, SchedulePointer } from "@/models";
import {
  ButtonGroup,
  FlexboxGrid,
  IconButton,
  List,
  Panel,
  Tooltip,
  Whisper,
} from "rsuite";
import { scheduleColorProvider } from "@/utils/ScheduleColorProvider";
import ScheduleSelector from "./ScheduleSelector";
import { ScheduleLabel } from "./ScheduleLabel";
import CloseIcon from "@rsuite/icons/Close";
import styles from "./SchedulesEditor.module.css";
import { useState } from "react";
import { SchedulesTags } from "./SchedulesTags";
import ModalMoreInformation from "./ScheduleHelpModals/ModalMoreInformation";
import ModalInstructions from "./ScheduleHelpModals/ModalInstructions";
import { serializeQuerySchedule } from "@/utils/ScheduleInSearchUtils";

interface SchedulesEditorProps {
  schedules: Schedule[];
  onScheduleRemove: (schedule: Schedule) => void;
  onScheduleSelected: (schedulePointer: SchedulePointer) => void;
}

export const SchedulesEditor = ({
  schedules,
  onScheduleRemove,
  onScheduleSelected,
}: SchedulesEditorProps) => {
  const [isPanelExpanded, setIsPanelExpanded] = useState(true);

  const panelHeader =
    schedules.length == 0 ? (
      <>
        Для просмотра индивидуального расписания введите группу, фамилию
        преподавателя или номер аудитории
      </>
    ) : isPanelExpanded ? (
      <>Расписания</>
    ) : (
      <div className={styles.panelScheduleTags}>
        <SchedulesTags schedules={schedules} />
      </div>
    );
  return (
    <>
      <Panel
        header={panelHeader}
        className={
          schedules.length == 0 ? "schedule_selector_attention" : undefined
        }
        bordered
        collapsible
        expanded={isPanelExpanded}
        onSelect={() => setIsPanelExpanded(!isPanelExpanded)}
      >
        <List bordered size="sm">
          {schedules.map((s, i) => (
            <List.Item
              key={i}
              style={{
                backgroundColor: scheduleColorProvider(i),
                color: "black",
              }}
            >
              <FlexboxGrid justify="space-between" align="middle">
                <FlexboxGrid.Item>
                  <ScheduleLabel schedule={s} />
                </FlexboxGrid.Item>
                <FlexboxGrid.Item>
                  <ButtonGroup size="md">
                    <ModalMoreInformation schedule={s} />
                    <ModalInstructions
                      href={s.icalLink}
                      scheduleTitle={s.title!}
                      scheduleId={serializeQuerySchedule(s)}
                    />
                    {!s.isStatic && (
                      <Whisper
                        trigger="hover"
                        placement={"auto"}
                        speaker={<Tooltip>Убрать из списка</Tooltip>}
                      >
                        <IconButton
                          icon={<CloseIcon />}
                          onClick={() => onScheduleRemove(s)}
                        />
                      </Whisper>
                    )}
                  </ButtonGroup>
                </FlexboxGrid.Item>
              </FlexboxGrid>
            </List.Item>
          ))}
        </List>
        <div className={styles.scheduleInput}>
          <ScheduleSelector
            disabled={schedules.length >= 3}
            schedules={schedules}
            onScheduleSelected={onScheduleSelected}
          />
        </div>
      </Panel>
    </>
  );
};
