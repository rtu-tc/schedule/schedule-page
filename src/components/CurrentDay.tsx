import { FlexboxGrid } from "rsuite";
import styles from "./CurrentDay.module.css";

type CurrentDateProps = {
  date: Date;
  size: "lg" | "md";
};

const stylesForSize = {
  date: {
    lg: styles.date_lg,
    md: styles.date_md,
  },
};

export const CurrentDate = ({ date, size }: CurrentDateProps) => {
  return (
    <FlexboxGrid align="middle" justify="center">
      <FlexboxGrid.Item>
        <p className={stylesForSize.date[size]}>
          {date.toLocaleDateString("ru-RU")}
        </p>
      </FlexboxGrid.Item>
    </FlexboxGrid>
  );
};
