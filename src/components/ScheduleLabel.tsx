import { ScheduleRow, ScheduleTarget } from "@/models";
import { ScheduleType } from "@/services/ScheduleAPI";
import { Location, Peoples, UserInfo } from "@rsuite/icons";

export const ScheduleLabel = ({
  schedule,
}: {
  schedule: ScheduleRow & {
    title: string | null;
    targetTitle?: string;
    fullTitle?: string;
  };
}) => {
  const title = schedule.fullTitle || schedule.title || schedule.targetTitle;
  return (
    <>
      {schedule.scheduleTarget === ScheduleType.Auditorium ? (
        <Location />
      ) : schedule.scheduleTarget === ScheduleType.Group ? (
        <Peoples />
      ) : schedule.scheduleTarget === ScheduleType.Teacher ? (
        <UserInfo />
      ) : (
        <></>
      )}{" "}
      {title}
    </>
  );
};
