import { Schedule } from "@/models";

import styles from "./SchedulesTags.module.css";
import { Tag } from "rsuite";
import { scheduleColorProvider } from "@/utils/ScheduleColorProvider";
import { ScheduleLabel } from "./ScheduleLabel";


interface SchedulesTagsProps {
  schedules: Schedule[];
}

export const SchedulesTags = ({ schedules }: SchedulesTagsProps) => {
  return (
    <div className={styles.schedulesTags}>
      {schedules.map((s, i) => (
        <Tag
          key={`${s.scheduleTarget}_${s.id}`}
          size="md"
          style={{
            backgroundColor: scheduleColorProvider(i),
            margin: "5px",
          }}
        >
          <ScheduleLabel schedule={s} />
        </Tag>
      ))}
    </div>
  );
};
