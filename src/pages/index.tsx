import Head from "next/head";
import React from "react";
import { useRef } from "react";
import { GetServerSideProps } from "next";
import ruRU from "rsuite/locales/ru_RU";
import { CustomProvider } from "rsuite";
import { CustomICalExpander } from "@/utils/CustomICalExpander";
import {
  Schedule,
  SchedulePointer,
  ScheduleRow,
  ScheduleTarget,
} from "@/models";
import TimeLine from "@/components/TimeLine";
import CompactCalendar from "@/components/CompactCalendar";
import {
  parseQuerySchedules,
  useSchedulePointersFromSearch,
  useScheduleDateFromSearch,
} from "@/utils/ScheduleInSearchUtils";
import { loadSchedule, schedulesSearch } from "@/services/ScheduleAPI";

import styles from "./index.module.css";
import { CurrentDate } from "@/components/CurrentDay";
import { SelectDateButtons } from "@/components/SelectDateButtons";
import { SchedulesEditor } from "@/components/SchedulesEditor";
import NotifyParentAboutDocumentSize from "@/components/NotifyParentAboutDocumentSize";

type HomeProps = {
  scheduleLoadInfo: ScheduleTarget[];
};
const useSchedules = (loaded: ScheduleTarget[]) => {
  const cache = useRef(new Map<string, Schedule>());
  return loaded.map((s) => {
    const key = `${s.scheduleTarget}_${s.id}`;
    const cachedSchedule = cache.current.get(key);
    if (cachedSchedule) {
      return cachedSchedule;
    }

    const calendar = new CustomICalExpander(s.iCalContent);

    const newSchedule: Schedule = {
      id: s.id,
      title: s.title,
      scheduleTarget: s.scheduleTarget,
      calendar: calendar,
      icalLink: s.iCalLink,
      isStatic: s.isStatic,
      iCalContent: s.iCalContent,
      iCalLink: s.iCalLink,
      scheduleImageLink: s.scheduleImageLink,
      scheduleUpdateImageLink: s.scheduleUpdateImageLink,
    };
    cache.current.set(key, newSchedule);
    return newSchedule;
  });
};

export const Home = ({ scheduleLoadInfo }: HomeProps) => {
  const [currentDate, setCurrentDate] = useScheduleDateFromSearch();
  const [schedulePointers, setSchedulePointers] =
    useSchedulePointersFromSearch();

  const pageTitle =
    scheduleLoadInfo.map((s) => s.title).join(" ") || "Расписание РТУ МИРЭА";
  const schedules = useSchedules(scheduleLoadInfo);

  const goToSelectedDate = (e: Date) => {
    setCurrentDate(e);
  };
  const scheduleSelected = (schedule: SchedulePointer) => {
    setSchedulePointers([
      ...schedulePointers,
      {
        id: schedule.id,
        scheduleTarget: schedule.scheduleTarget,
      },
    ]);
  };

  const scheduleRemoved = (schedule: Schedule) => {
    setSchedulePointers(
      schedulePointers.filter(
        (s) =>
          s.id != schedule.id || s.scheduleTarget != schedule.scheduleTarget
      )
    );
  };

  return (
    <>
      <NotifyParentAboutDocumentSize />
      <CustomProvider locale={ruRU} theme="light">
        <link rel="icon" href="/favicon.ico" />
        <Head>
          <title>{pageTitle}</title>
          <meta name="description" content="Расписание РТУ МИРЭА" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
        </Head>

        <div className={styles.page}>
          <div className={styles.controls}>
            <div className={styles.currentDay}>
              <CurrentDate date={currentDate} size="lg" />
            </div>

            <div className={styles.calendar}>
              <CompactCalendar
                currentDate={currentDate}
                onSelectDateEvent={goToSelectedDate}
                schedules={schedules}
              />
            </div>
            <div className={styles.selectDateButton}>
              <SelectDateButtons
                date={currentDate}
                setDate={goToSelectedDate}
                schedules={schedules}
              />
            </div>

            <div className={styles.selectedSchedules}>
              <SchedulesEditor
                schedules={schedules}
                onScheduleSelected={scheduleSelected}
                onScheduleRemove={scheduleRemoved}
              />
            </div>
          </div>
          <div className={styles.timelLine}>
            <TimeLine schedules={schedules} date={currentDate} />
          </div>
        </div>
      </CustomProvider>
    </>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async (context) => {
  let scheduleSearchedInfo = parseQuerySchedules(
    context.query["s"]?.toString() || null
  ).map((s) => ({ ...s, isStatic: false } as ScheduleRow));

  scheduleSearchedInfo = await populateWithScheduleTitle(
    scheduleSearchedInfo,
    context.query["scheduleTitle"]?.toString() || null
  );
  const schedulesDayView = await Promise.all(
    scheduleSearchedInfo.map(async (e, i) => {
      const scheduleInfo = await loadSchedule(e.scheduleTarget, e.id);
      if (!scheduleInfo) {
        return null;
      }
      const scheduleObject: ScheduleTarget = {
        id: e.id,
        scheduleTarget: e.scheduleTarget,
        isStatic: e.isStatic || false,
        title: scheduleInfo.schedule.targetTitle,
        iCalContent: scheduleInfo.content,
        iCalLink: scheduleInfo.schedule.iCalLink,
        scheduleImageLink: scheduleInfo.schedule.scheduleImageLink,
        scheduleUpdateImageLink: scheduleInfo.schedule.scheduleUpdateImageLink,
      };
      return scheduleObject;
    })
  );
  return {
    props: {
      scheduleLoadInfo: schedulesDayView.filter(v => v),
    },
  };
};
const populateWithScheduleTitle = async (
  sourceSchedules: ScheduleRow[],
  scheduleTitle: string | null
) => {
  if (!scheduleTitle) {
    return sourceSchedules;
  }
  const searchResult = await schedulesSearch(scheduleTitle);
  // Если найдено больше одного или нет вовсе - ничего не прикладываем, как этот функционал должен отвечать условию уникальности
  const targetSchedule =
    searchResult.data.length === 1 ? searchResult.data[0] : null;
  if (!targetSchedule) {
    return sourceSchedules;
  }
  return [
    {
      id: targetSchedule.id,
      scheduleTarget: targetSchedule.scheduleTarget,
      isStatic: true,
    } as ScheduleRow,
    ...sourceSchedules.filter(
      (ss) =>
        ss.id != targetSchedule.id ||
        ss.scheduleTarget != targetSchedule.scheduleTarget
    ),
  ];
};
