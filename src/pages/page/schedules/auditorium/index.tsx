import Head from "next/head";
import React from "react";
import { useState, useEffect } from "react";
import { Table } from "antd";
import ruRU from "rsuite/locales/ru_RU";
import { CustomProvider } from "rsuite";
import { CustomICalExpander } from "@/utils/CustomICalExpander";
import { loadSchedule, schedulesSearch } from "@/services/ScheduleAPI";
import { GetServerSideProps } from "next";

import styles from "./index.module.css";

type AuditoriumInfoProps = {
  auditorium: string,
  auditoriumInfo: [];
};

export const AuditoriumInfo = ({auditorium, auditoriumInfo}: AuditoriumInfoProps) => {
  const [currentDate, setCurrentDate] = useState<Date>(new Date());
  const [currentTime, setCurrentTime] = useState<string>();
  const [rowId, setRowId] = useState<number>();
  
  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setCurrentDate(date);
      getBackgroundColor()
      const minutes = (date.getMinutes()<10) ? `0${date.getMinutes()}` : `${date.getMinutes()}`
      setCurrentTime(`${date.getHours()}:${minutes}`)
    }, 30000);
  });


  const columns = [
    {
      title: "№ ПАРЫ",
      dataIndex: "number"
    },
    {
      title: "ПРЕДМЕТ",
      dataIndex: "item"
    },
    {
      title: "ПРЕПОДАВАТЕЛЬ",
      dataIndex: "teacher"
    },
    {
      title: "ГРУППЫ",
      dataIndex: "groups"
    }
  ];
  
  const setTime = (timeHours: number, timeMinutes: number) => {
    return new Date(new Date((new Date(currentDate.setHours(timeHours))).setMinutes(timeMinutes)).setSeconds(0))
  }
  
  const getBackgroundColor = () => {
      const currentTime = currentDate.getTime()
      if (setTime(9, 0).getTime() <= currentTime && currentTime <= setTime(10, 30).getTime()){
        setRowId(0)
      } else if (setTime(10, 40).getTime() <= currentTime && currentTime <= setTime(12, 10).getTime()) {
        setRowId(1)
      } else if (setTime(12, 40).getTime() <= currentTime && currentTime <= setTime(14, 10).getTime()) {
        setRowId(2)
      } else if (setTime(14, 20).getTime() <= currentTime && currentTime <= setTime(15, 50).getTime()) {
        setRowId(3)
      } else if (setTime(16, 20).getTime() <= currentTime && currentTime <= setTime(17, 50).getTime()) {
        setRowId(4)
      } else if (setTime(18, 0).getTime() <= currentTime && currentTime <= setTime(19, 30).getTime()) {
      } else {
        // console.warn('Время вне интервала')
      }
  }

  const setRowClassName = (record: any) => {
    return record.key === rowId ? styles.activeRow : '';
  }

  return (
    <>
      <CustomProvider locale={ruRU} theme="light">
         <link rel="icon" href="/favicon.ico" />
         <Head>
          <title>Информация на мониторах</title>
          <meta name="description" content="Расписание РТУ МИРЭА" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
         </Head>
        <div className={styles.page}>
          <div className={styles.currentDayTable}>
              Расписание аудитории {auditorium} на {currentDate.toLocaleDateString("ru-RU")} {currentTime}
          </div>
          <div className={styles.table}>
            <Table
              pagination={false}
              columns={columns}
              dataSource={auditoriumInfo}
              rowClassName={(e) => setRowClassName(e)}
            />
          </div>
        </div>
      </CustomProvider>
    </>
  );
};

export default AuditoriumInfo;



export const getServerSideProps: GetServerSideProps = async (context) => {
  const paramQuery = context.query["auditorium"]?.toString() || '';
  
  const auditorium = await schedulesSearch(paramQuery);
  const auditorium_id = (auditorium.data[0]) ? auditorium.data[0].id : 100000;
  const auditorium_sheduleTarget = (auditorium.data[0]) ? auditorium.data[0].scheduleTarget : 0;

  const auditorium_info = await loadSchedule(auditorium_sheduleTarget, auditorium_id);
  const auditorium_calendar = new CustomICalExpander(auditorium_info?.content || '');
  const auditorium_element = auditorium_calendar.getEventsForDay(new Date())

  const getIndex = (date: Date) => {
      const timeHours = date.getHours()
      const timeMinutes = date.getMinutes()
      const full = (timeMinutes < 10) ? `${timeHours}:0${timeMinutes}` : `${timeHours}:${timeMinutes}`
      switch (full){
        case "9:00": return 0
        case "10:40": return 1
        case "12:40": return 2
        case "14:20": return 3
        case "16:20": return 4
        case "18:00": return 5
        default: return -1
      }
  }

  const getDescription = (description: string) => {
    const descriptionFull = description.split(':')
    let teacher , groups;
    if (descriptionFull.length == 1){
      teacher = ""
      groups = descriptionFull[0] || ""
    } else {
      const teacherFull = (descriptionFull[1]) ? descriptionFull[1].split(`Групп`) : ""
      teacher = teacherFull[0]
      groups = descriptionFull[2] || ""
    }
    return [teacher, groups]
  }
  const array_table_full: any[] = []
  const array_table: any[] = []
  const mas_time: number[] = []

  const time = ["(09:00 - 10:30)", "(10:40 - 12:10)", "(12:40 - 14:10)", "(14:20 - 15:50)", "(16:20 - 17:50)", "(18:00-19:30)" ]
  auditorium_element.map((e) => {
    if (!mas_time.includes(e.start.getTime())){
      mas_time.push(e.start.getTime())
      const keys = getIndex(e.start)
      array_table.push({
        key: keys, 
        number: `${keys + 1} ${time[keys]}`,
        item: e.title,
        teacher: getDescription(e.description)[0],
        groups: getDescription(e.description)[1],
      })
    } else {
      const indexFirst = mas_time.indexOf(e.start.getTime(), 0)
      array_table[indexFirst].item = `${array_table[indexFirst].item}, ${e.title}`
      array_table[indexFirst].teacher = `${array_table[indexFirst].teacher} ${getDescription(e.description)[0]}` 
      array_table[indexFirst].groups = `${array_table[indexFirst].groups} ${getDescription(e.description)[1]}` 
    }
  })

  for (let i = 0; i < 6; i++) {
      const el = array_table.filter((lessson) => lessson.key == i)
      if (el.length == 0){
        array_table_full.push({
               key: i,
               number: `${i + 1} ${time[i]}`,
               item: "",
               teacher: "",
               groups: "",
               start: "",
        })
      } else {
        array_table_full.push(el[0])
      }
  }


  return {
    props: {
      auditorium: paramQuery,
      auditoriumInfo: array_table_full,
    },
  };
};