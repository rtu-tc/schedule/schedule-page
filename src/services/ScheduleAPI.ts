import { Configuration } from "@/utils/Configuration"

export const schedulesSearch: (match: string) => Promise<ListResponse<ScheduleEntry>> = async (match) => {
  var result = await fetch(`${Configuration.ScheduleBaseAddress}/schedule/api/search?limit=15&match=${encodeURIComponent(match)}`);
  return await result.json();
}

export const loadSchedule: (type: ScheduleType, id: number) => Promise<{ content: string, schedule: ScheduleEntry } | null>
  = async (type: ScheduleType, id: number) => {

    const scheduleInfoResponse = await fetch(`${Configuration.ScheduleBaseAddress}/schedule/api/baseinfo?id=${id}&type=${type}`);
    if (scheduleInfoResponse.status !== 200) {
      console.error("server can't return schedule base info", scheduleInfoResponse.status)
      return null;
    }
    const schedule = await scheduleInfoResponse.json() as ScheduleEntry;
    const link = schedule.iCalLink;
    
    
    const icsResponse = await fetch(link, {
      headers: {
        'Client-Name': Configuration.ScheduleApiClientName,
      }
    });
    if (icsResponse.status !== 200) {
      console.error("server can't return ical", icsResponse.status)
      return null;
    }
    const content = await icsResponse.text();
    return { content, schedule }
  };


export interface ListResponse<T> {
  data: T[]
}

export enum ScheduleType {
  Unknown = 0,
  Group = 1,
  Teacher = 2,
  Auditorium = 3
}

export interface ScheduleEntry {
  id: number
  targetTitle: string
  fullTitle: string
  scheduleTarget: ScheduleType
  iCalLink: string
  scheduleImageLink: string
  scheduleUpdateImageLink: string
}
