export class Configuration {
  public static get ScheduleBaseAddress(): string {
    return process.env.SCHEDULE_BASE_ADDRESS || "";
  }
  public static get ScheduleApiClientName(): string {
    return process.env.NEXT_PUBLIC_SCHEDULE_API_CLIENT_NAME || "";
  }
}