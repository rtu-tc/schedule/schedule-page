import { SchedulePointer, ScheduleRow } from "@/models";
import { ScheduleType } from "@/services/ScheduleAPI";
import moment from "moment-timezone";
import { ReadonlyURLSearchParams, usePathname, useSearchParams } from "next/navigation";
import { useRouter } from "next/router";
import { useMemo } from "react";


const transformmDataFormat = (dateOldFormat: Date) => {
  const dateYear = dateOldFormat.getFullYear();
  const dateMonth = dateOldFormat.getMonth() + 1;
  const dateDay = dateOldFormat.getDate();
  const dateFull = [dateYear, dateMonth, dateDay].join('-')
  return dateFull
}

export const createGoToPath = (pathname: string, searchParams: ReadonlyURLSearchParams, values: { [key: string]: string }) => {
  const params = new URLSearchParams(searchParams.toString()); // TODO: check status of https://github.com/vercel/next.js/issues/49245
  for (const key in values) {
    if (Object.prototype.hasOwnProperty.call(values, key)) {
      const value = values[key];
      if (value) {
        params.set(key, value);
      } else {
        params.delete(key);
      }
    }
  }

  let query = params.toString();
  if (query) {
    query = "?" + query;
  }
  return pathname + query;
};


export const parseQuerySchedule: (serialized: string) => SchedulePointer | null
  = (serialized) => {
    const splitted = serialized.split("_");
    if (splitted.length !== 2) {
      return null;
    }
    const scheduleTarget = +splitted[0];

    if (!Object.values(ScheduleType).includes(scheduleTarget)) {
      return null;
    }

    const id = +splitted[1];
    return {
      id, scheduleTarget
    };
  };
export const parseQuerySchedules: (value: string | null) => SchedulePointer[] = (value) => {
  if (!value) {
    return [];
  }
  const result: SchedulePointer[] = [];
  const schedules = value.split(",");
  schedules.forEach(s => {
    if (!s) {
      return;
    }
    const pointer = parseQuerySchedule(s);
    if (pointer) {
      result.push(pointer);
    }
  });
  return result;
}


export const serializeQuerySchedule: (schedule: SchedulePointer) => string
  = (schedule) => `${schedule.scheduleTarget}_${schedule.id}`;
export const serializeQuerySchedules: (schedules: SchedulePointer[]) => string
  = (schedules) => schedules.map(serializeQuerySchedule).join(",");

export const useScheduleDateFromSearch: () => [Date, (currentDate: Date) => void] = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const date = searchParams.get("date");

  let currentDate: Date
  if (date) {
    currentDate = moment(date, 'YYYY-M-D').toDate();
  } else {
    currentDate = new Date()
  }

  const setCurrentDate = (date: Date) => {
    const urlView = transformmDataFormat(date);
    router.push(createGoToPath(pathname, searchParams, { date: urlView }), undefined, { shallow: true });
  }

  return [currentDate, setCurrentDate];
}

export const useSchedulePointersFromSearch: () => [SchedulePointer[], (pointers: SchedulePointer[]) => void] = () => {
  
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const schedulesFromSearch = searchParams.get("s");

  const setPointers = (pointers: SchedulePointer[]) => {
    const urlView = serializeQuerySchedules(pointers);
    router.push(createGoToPath(pathname, searchParams, { s: urlView }));
  }

  const cachedFromUrl = useMemo(() => parseQuerySchedules(schedulesFromSearch), [schedulesFromSearch]);
  return [cachedFromUrl, setPointers];
}